# Heracles 

## Herramienta para realizar pruebas de autenticación

### Ejemplo de uso:

HELP:
    heracles -h

FTP:

    heracles -s ftp -u user -p pass -H 127.0.0.1

TELNET:

    heracles -s telnet -u user -p pass -H 127.0.0.1

SMTP:
    
    heracles -s smtp -u user -p pass -H 127.0.0.1

RSHELL

    heracles -s rshell -u user -p pass -H 127.0.0.1
                  
IMAP

    heracles -s imap -u test@test.com -p 12345678 -H 127.0.0.1

POP3

    heracles -s pop3 -u user -p pass -H 127.0.0.1

SSH

    heracles -s ssh -u user -p pass -H 127.0.0.1

FTP    
    
    heracles -s ftp -u user -p pass -H 127.0.0.1

HTTP

    heracles -s http -u test,root -p 1234,owaspbwa -H 127.0.0.1 -hac WebGoat/attack

    heracles -s http -hau form -hm post -hac dvwa/login.php -hp username=$USER$ -hp password=$PASS$ -hp Login=Login -hid failed -u test,root,admin -p 1234,owaspbwa,admin -H 127.0.0.1
            
    heracles -s http -hau form -hm get_post -hac dvwa/login.php -u user,admin -p user,admin -H 127.0.0.1

    heracles -s http -hau form -hm get_post -hac bWAPP/login.php -u user,admin,bee -p admin,bug -H 127.0.0.1 -t 1 -hpr 127.0.0.1:8080


Linux Instalación

    Sobre la carpeta /opt copiar el archivo heracles.zip

    Ejecutar los siguiente comando sobre la carpeta /opt:

    unzip heracles.zip

    rm heracles.zip
    
    chmod -R 755 heracles

    cd heracles

    cp heracles.sh /usr/bin/heracles