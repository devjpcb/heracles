package com.devjpcb.ssl;

import java.io.IOException;

import javax.net.ssl.SSLContext;

import org.apache.commons.net.util.SSLContextUtils;

public class SSLUtils {
    public static SSLContext GetSSLContext() {
        SSLContext context = null;
        
        try {
            context = SSLContextUtils.createSSLContext("TLS", null, new SkipTrustManager());
        } catch(IOException ex) {}

        return context; 
    }
}
