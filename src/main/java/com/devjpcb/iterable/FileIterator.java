package com.devjpcb.iterable;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

public class FileIterator implements Iterator<String> {
    private Scanner scanner;

    public FileIterator(File file) {
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) { }
    }

    @Override
    public boolean hasNext() {
        boolean next = false;

        if (scanner != null ) {
            next = scanner.hasNextLine();
            if (!next)
                scanner.close();
        }

        return next;
    }

    @Override
    public String next() {
        if (scanner != null)
            return scanner.nextLine();
        return null;
    }
}
