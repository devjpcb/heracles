package com.devjpcb.iterable;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class CommandOptionIterable implements Iterable<String> {
    private String option;

    public CommandOptionIterable(String option) {
        this.option = option;
    }

    @Override
    public Iterator<String> iterator() {
        var file = new File(option);

        if (file.exists()) {
            return new FileIterator(file);
        }

        var list = new ArrayList<String>();
        
        for (var line : option.split(","))
            if (line.length() > 0)
                list.add(line);

        return list.iterator();
    }
}

