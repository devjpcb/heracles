package com.devjpcb;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.devjpcb.iterable.CommandOptionIterable;
import com.devjpcb.service.RunnableService;
import com.devjpcb.service.RunnableServiceParameters;
import com.devjpcb.service.Services;

import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Options;

public class Heracles {
    public static void main(String[] args) {
        var version = 1.0;
        var year = LocalDate.now().getYear();

        System.out.println("Heracles " + version + " (c) " + year + " by devjpcb - Authentication test tool");

        var services = new Services();

        var options = new Options();
        options.addOption("h", "help", false, "print help message");
        options.addRequiredOption("u", "user", true, "comma separated usernames or file with username list");
        options.addOption("p", "pass", true, "comma separated passwords or file with password list");
        options.addRequiredOption("H", "host", true, "hostname or IP");
        options.addOption("P", "port", true, "port number");
        options.addRequiredOption("s", "service", true, String.format("services: %s", services.getNameServices()));
        options.addOption("ss", "subservice", true, String.format("subservice: %s", services.getNameSubServices()));
        options.addOption("t", "threads", true, "number of connects in parallel");
        options.addOption("v", "verbose", false, "show username and password for each attempt");
        options.addOption("eet", "enable_explicit_tls", false, "enable ssl/tls in explicit mode");
        options.addOption("eit", "enable_implicit_tls", false, "enable ssl/tls in implicit mode");

        services.prepareOptions(options);

        try {
            var cmdparser = new DefaultParser();
            var cmdline = cmdparser.parse(options, args);

            if (args.length == 0 || cmdline.hasOption("help")) {
                showHelp(options);
                return;
            }

            var serviceOption = cmdline.getOptionValue("service");

            var service = services.getService(serviceOption);

            if (service == null)
                throw new NullPointerException("not found service");

            service.setCmdLine(cmdline);
            service.setup();

            final var hasPassword = service.hasPassword();

            if (hasPassword && !cmdline.hasOption("p"))
                throw new MissingOptionException(List.of("p"));

            var userOption = cmdline.getOptionValue("user");
            var passOption = cmdline.getOptionValue("pass");
            var threadsOption = Integer.parseInt(cmdline.getOptionValue("threads", "10"));

            var executorIndex = new AtomicInteger(0);
            var executorService = Executors.newFixedThreadPool(threadsOption);

            var start = LocalDateTime.now();

            System.out.printf("Start: %s\n", start);

            for (var user : new CommandOptionIterable(userOption)) {
                if (hasPassword) {
                    for (var pass : new CommandOptionIterable(passOption)) {
                        var parameters = new RunnableServiceParameters();
                        parameters.setService(service);
                        parameters.setExecutorIndex(executorIndex);
                        parameters.setUser(user);
                        parameters.setPass(pass);
                        parameters.setExecutorService(executorService);
                        
                        executorService.execute(new RunnableService(parameters));
                    }
                }
                else {
                    var parameters = new RunnableServiceParameters();
                    parameters.setService(service);
                    parameters.setExecutorIndex(executorIndex);
                    parameters.setUser(user);
                    parameters.setExecutorService(executorService);

                    executorService.execute(new RunnableService(parameters));
                }
            }
            
            executorService.shutdown();
            
            try {
                executorService.awaitTermination(1, TimeUnit.MINUTES);
            } catch (InterruptedException e) { }

            var end = LocalDateTime.now();

            var diff = ChronoUnit.MILLIS.between(start, end);
            
            System.out.printf("End: %s, Total Milliseconds: %d\n", end, diff);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            showHelp(options);
        }
    }

    private static void showHelp(Options options) {
        var formatter = new HelpFormatter();
        formatter.setOptionComparator(null);
        formatter.setWidth(148);
        formatter.printHelp("heracles", options);
    }
}