package com.devjpcb.service.pop3;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;
import com.devjpcb.ssl.SSLUtils;

import org.apache.commons.net.pop3.POP3Client;
import org.apache.commons.net.pop3.POP3SClient;

public class POP3Service extends AbstractService {
    @Override
    public String getDescription() {
        return "support for pop3 and pop3 over ssl/tls";
    }

    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var explicit = getCmdLine().hasOption("enable_explicit_tls");
        var implicit = getCmdLine().hasOption("enable_implicit_tls");

        POP3Client pop3Client = null;

        if (explicit)
            pop3Client = new POP3SClient(false, SSLUtils.GetSSLContext());
        else if (implicit)
            pop3Client = new POP3SClient(true, SSLUtils.GetSSLContext());
        else
            pop3Client = new POP3Client();

        try {            
            pop3Client.connect(getHost(), getPort(pop3Client.getDefaultPort()));
            if (explicit) {
                var imapsClient = (POP3SClient)pop3Client;
                if (!imapsClient.execTLS())
                    throw new Exception("Explicit TLS not supported");
            }
            var result = pop3Client.login(user, pass);
            if (result)
                pop3Client.logout();
            return result;
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (pop3Client.isConnected()) {
                try {
                    pop3Client.disconnect();
                } catch (IOException e) { }
            }
        }
    }
}
