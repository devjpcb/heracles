package com.devjpcb.service.imap;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;
import com.devjpcb.ssl.SSLUtils;

import org.apache.commons.net.imap.IMAPClient;
import org.apache.commons.net.imap.IMAPSClient;

public class IMAPService extends AbstractService {
    @Override
    public String getDescription() {
        return "support for imap and imap over ssl/tls";
    }

    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var explicit = getCmdLine().hasOption("enable_explicit_tls");
        var implicit = getCmdLine().hasOption("enable_implicit_tls");

        IMAPClient imapClient = null;

        if (explicit)
            imapClient = new IMAPSClient(false, SSLUtils.GetSSLContext());
        else if (implicit)
            imapClient = new IMAPSClient(true, SSLUtils.GetSSLContext());
        else
            imapClient = new IMAPClient();

        try {            
            var defaultPort = implicit ? IMAPSClient.DEFAULT_IMAPS_PORT : IMAPClient.DEFAULT_PORT;
            imapClient.connect(getHost(), getPort(defaultPort));
            if (explicit) {
                var imapsClient = (IMAPSClient)imapClient;
                if (!imapsClient.execTLS())
                    throw new Exception("Explicit TLS not supported");
            }
            var result = imapClient.login(user, pass);
            if (result)
                imapClient.logout();
            return result;
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (imapClient.isConnected()) {
                try {
                    imapClient.disconnect();
                } catch (IOException e) { }
            }
        }
    }
    
}
