package com.devjpcb.service.ftp;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;
import com.devjpcb.ssl.SSLUtils;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;

public class FTPService extends AbstractService {
    @Override
    public String getDescription() {
        return "support for ftp and ftp over ssl/tls";
    }

    @Override
    public boolean performTest(String username, String password) throws ServiceException {
        var explicit = getCmdLine().hasOption("enable_explicit_tls");
        var implicit = getCmdLine().hasOption("enable_implicit_tls");

        FTPClient ftpClient = null;

        if (explicit)
            ftpClient = new FTPSClient(false, SSLUtils.GetSSLContext());
        else if (implicit)
            ftpClient = new FTPSClient(true, SSLUtils.GetSSLContext());
        else
            ftpClient = new FTPClient();

        try {
            ftpClient.connect(getHost(), getPort(ftpClient.getDefaultPort()));

            var result = ftpClient.login(username, password);

            if (result)
                ftpClient.logout();
            
            return result;
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException e) { }
            }
        }
    }
}
