package com.devjpcb.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public class RunnableServiceParameters {
    private Service service;
    private AtomicInteger executorIndex;
    private String user;
    private String pass;
    private ExecutorService executorService;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public AtomicInteger getExecutorIndex() {
        return executorIndex;
    }

    public void setExecutorIndex(AtomicInteger executorIndex) {
        this.executorIndex = executorIndex;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }    
}
