package com.devjpcb.service;

import java.util.ArrayList;
import java.util.List;

import com.devjpcb.service.bsd.RExecService;
import com.devjpcb.service.bsd.RLoginService;
import com.devjpcb.service.bsd.RShellService;
import com.devjpcb.service.ftp.FTPService;
import com.devjpcb.service.http.HTTPService;
import com.devjpcb.service.imap.IMAPService;
import com.devjpcb.service.jdbc.JDBCService;
import com.devjpcb.service.pop3.POP3Service;
import com.devjpcb.service.smtp.SMTPService;
import com.devjpcb.service.ssh.SSHService;
import com.devjpcb.service.telnet.TelnetService;

import org.apache.commons.cli.Options;

public class Services {
    private List<Service> services;

    public Services() {
        services = new ArrayList<Service>();
        services.add(new FTPService());
        services.add(new HTTPService());
        services.add(new IMAPService());
        services.add(new POP3Service());
        services.add(new RExecService());
        services.add(new RLoginService());
        services.add(new RShellService());
        services.add(new SMTPService());
        services.add(new TelnetService());
        services.add(new SSHService());
        services.add(new JDBCService());
    }

    public String getNameServices() {
        var nameServices = new ArrayList<String>(); 

        for (var index = 0; index < services.size(); index++) {
            var position = index + 1;
            var service = services.get(index);    
            nameServices.add(String.format("%d. %s: %s", position, service.getName(), service.getDescription()));
        }

        return System.lineSeparator() + String.join(System.lineSeparator(), nameServices);
    }

    public String getNameSubServices() {
        var nameServices = new ArrayList<String>(); 

        for (var service : services) {
            var subServices = service.subServices();

            if (subServices.size() > 0) {
                nameServices.add(String.format("%s: %s", service.getName(), String.join(",", subServices)));
            }
        }

        return System.lineSeparator() + String.join(System.lineSeparator(), nameServices);
    }

    public void prepareOptions(Options options) {
        for (var service : services)
            service.prepareOptions(options);
    }

    public Service getService(String name) {
        for (var service : services)
            if (service.getName().equals(name))
                return service;
        
        return null;
    }
}