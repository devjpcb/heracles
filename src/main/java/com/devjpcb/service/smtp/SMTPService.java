package com.devjpcb.service.smtp;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;
import com.devjpcb.ssl.SSLUtils;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.net.smtp.AuthenticatingSMTPClient;
import org.apache.commons.net.smtp.SMTPReply;

public class SMTPService extends AbstractService {
    @Override
    public String getDescription() {
        return "support for smtp and smtp over ssl/tls";
    }

    @Override
    public void prepareOptions(Options options) {
        options.addOption(new Option("sv", "smtp_verify", false, "verify that a username or email address is valid (vrfy/expn/rcpt)"));
    }
    
    @Override
    public boolean hasPassword() {
        return !getCmdLine().hasOption("smtp_verify");
    }

    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var explicit = getCmdLine().hasOption("enable_explicit_tls");
        var implicit = getCmdLine().hasOption("enable_implicit_tls");
        var smtpClient = new AuthenticatingSMTPClient(implicit, SSLUtils.GetSSLContext());

        try {
            smtpClient.connect(getHost(), getPort(smtpClient.getDefaultPort()));
            smtpClient.login();
            if (explicit) {
                if (!smtpClient.execTLS())
                    throw new Exception("Explicit TLS not supported");
            }
            var result = false;
            if (getCmdLine().hasOption("smtp_verify")) {
                result = verify(smtpClient, user);
            } else {
                result = smtpClient.auth(AuthenticatingSMTPClient.AUTH_METHOD.LOGIN, user, pass);
            }
            smtpClient.logout();
            return result;
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (smtpClient.isConnected()) {
                try {
                    smtpClient.disconnect();
                } catch (IOException e) {
                }
            }
        }
    }

    private boolean verify(AuthenticatingSMTPClient smtpClient, String username) throws IOException {
        int vrfyCode, expnCode, rcptCode;
        vrfyCode = smtpClient.vrfy(username);
        expnCode = smtpClient.expn(username);
        rcptCode = smtpClient.rcpt(username);
        
        return vrfyCode == SMTPReply.ACTION_OK || vrfyCode == SMTPReply.USER_NOT_LOCAL_WILL_FORWARD ||
            expnCode == SMTPReply.ACTION_OK || expnCode == SMTPReply.USER_NOT_LOCAL_WILL_FORWARD ||
            rcptCode == SMTPReply.ACTION_OK || rcptCode == SMTPReply.USER_NOT_LOCAL_WILL_FORWARD;
    }
}
