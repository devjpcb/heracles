package com.devjpcb.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

public abstract class AbstractService implements Service {
    private CommandLine cmdline;

    @Override
    public String getName() {
        var simpleName = this.getClass().getSimpleName();
        var name = simpleName.substring(0, simpleName.indexOf("Service"));
        return name.toLowerCase();
    }

    @Override
    public String getDescription() {
        return "support for " + getName();
    }

    @Override
    public void prepareOptions(Options options) {

    }
    
    @Override
    public void setCmdLine(CommandLine cmdline) {
        this.cmdline = cmdline;
    }

    @Override
    public void setup() throws ServiceException {
        
    }
    
    @Override
    public CommandLine getCmdLine() {
        return cmdline;
    }

    @Override
    public boolean hasPassword() {
        return true;
    }

    protected String getHost() {
        var hostOption = getCmdLine().getOptionValue("host");
        return hostOption;
    }

    protected int getPort(int defaultPort) {
        var defaultPortOption = String.valueOf(defaultPort);
        var portOption = getCmdLine().getOptionValue("port", defaultPortOption);
        return Integer.parseInt(portOption);
    }

    @Override
    public List<String> subServices() {
        return new ArrayList<>();
    }
}
