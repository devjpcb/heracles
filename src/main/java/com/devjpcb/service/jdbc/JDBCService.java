package com.devjpcb.service.jdbc;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.Options;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;

public class JDBCService extends AbstractService {
    private List<String> subServices = List.of(
            "postgresql",
            "sqlserver",
            "mysql");

    private Map<String, Map<String, String>> properties = Map.of(
            "postgresql", Map.of(
                    "driver", "org.postgresql.Driver",
                    "url", "jdbc:postgresql://%s:%s/%s",
                    "port", "5432"),
            "sqlserver", Map.of(
                    "driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver",
                    "url", "jdbc:sqlserver://%s:%s;encrypt=false;databaseName=%s",
                    "port", "1433"),
            "mysql", Map.of(
                    "driver", "com.mysql.cj.jdbc.Driver",
                    "url", "jdbc:mysql://%s:%s/%s",
                    "port", "3306"));

    @Override
    public List<String> subServices() {
        return subServices;
    }

    @Override
    public void setup() throws ServiceException {
        var subservice = getCmdLine().getOptionValue("subservice");

        if (subservice == null)
            throw new NullPointerException("not found subservice");

        var driver = properties.get(subservice).get("driver");

        if (!driver.isEmpty()) {
            try {
                Class.forName(driver);
            } catch (ClassNotFoundException ex) {
                throw new ServiceException(ex.getMessage());
            }
        }
    }

    @Override
    public void prepareOptions(Options options) {
        options.addOption("db", "database", true, "database name");
    }

    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        try {
            var subservice = getCmdLine().getOptionValue("subservice");

            var url = properties.get(subservice).get("url");
            var port = properties.get(subservice).get("port");

            var db = getCmdLine().getOptionValue("database", "");

            url = String.format(url, getHost(), getPort(Integer.parseInt(port)), db);

            try (var con = DriverManager.getConnection(url, user, pass)) {
                return true;
            }
        } catch (Exception ex) {
            if (ex instanceof SQLException sqlex) {
                var sqlstate = sqlex.getSQLState();

                switch (sqlstate) {
                    case "28P01":
                    case "S0001":
                    case "28000":
                        return false;
                }
            }

            throw new ServiceException(ex.getMessage());
        }
    }
}
