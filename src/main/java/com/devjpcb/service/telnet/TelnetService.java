package com.devjpcb.service.telnet;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;

import org.apache.commons.net.telnet.TelnetClient;

public class TelnetService extends AbstractService {
    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var telnetClient = new TelnetClient();
        var telnetReadWrite = new TelnetReadWrite(telnetClient);   
        
        try {
            telnetClient.connect(getHost(), getPort(telnetClient.getDefaultPort()));

            String data = telnetReadWrite.readUntil("login:");

            if (data.contains("login:")) {
                telnetReadWrite.write(user);
            }

            data = telnetReadWrite.readUntil("password:");

            if (data.contains("password:")) {
                telnetReadWrite.write(pass);
            }

            var loginKey = user.toLowerCase();

            data = telnetReadWrite.readUntil(String.format("incorrect|invalid|failed|%s", loginKey));

            return data.contains(loginKey);
        } catch(Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (telnetClient.isConnected()) {
                try {
                    telnetClient.disconnect();
                } catch (IOException e) { }
            }
        }
    }
}
