package com.devjpcb.service.telnet;

import java.io.IOException;

import org.apache.commons.net.telnet.TelnetClient;

public class TelnetReadWrite {
    private TelnetClient telnetClient;

    public TelnetReadWrite(TelnetClient telnetClient) {
        this.telnetClient = telnetClient;
    }

    public String readUntil(String breakKeys) throws IOException {
        var bs = new StringBuilder();
        var c = 0;
        var is = telnetClient.getInputStream();

        String[] keys = breakKeys.split("\\|");
        char[] lastchar = new char[keys.length];

        for (var i = 0; i < keys.length; i++)
            lastchar[i] = keys[i].charAt(keys[i].length() - 1);

        while ((c = is.read()) >= 0) {
            var breakLoop = false;

            bs.append((char)c);
            
            for (var i = 0; i < keys.length; i++) {
                if ((char)c == lastchar[i]) {
                    if (bs.toString().toLowerCase().endsWith(keys[i])) {
                        breakLoop = true;
                    }
                } 
            }

            if (breakLoop)
                break;
        }

        return bs.toString().toLowerCase();
    }

    public void write(String string)  throws IOException {
        var os = telnetClient.getOutputStream();

        for (var c : string.toCharArray()) {
            os.write((int)c);
        }

        os.write('\r');
        os.write('\n');
        os.flush();
    }
}
