package com.devjpcb.service.http;

import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Base64;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;
import com.devjpcb.ssl.SSLUtils;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.jsoup.Jsoup;

public class HTTPService extends AbstractService {
    @Override
    public String getDescription() {
        return "support for http and http over ssl/tls";
    }

    @Override
    public void prepareOptions(Options options) {
        options.addOption("hau", "http_auth", true, "authentication mode: basic, form");
        options.addOption("hm", "http_method", true, "http method: get, post, get_post (mixed mode)");
        options.addOption("hac", "http_action", true, "http action");
        options.addOption(Option.builder("hh").longOpt("http_header").argName("property=value").hasArgs()
                .valueSeparator().desc("http header").build());
        options.addOption(Option.builder("hp").longOpt("http_parameter").argName("property=value").hasArgs()
                .valueSeparator().desc("http parameter").build());
        options.addOption("hid", "http_invalid_data", true, "invalid data on the http response");
        options.addOption("hpr", "http_proxy", true, "http proxy");
    }

    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        try {
            var implicit = getCmdLine().hasOption("enable_implicit_tls");
            var prot = implicit ? "https" : "http";
            var host = getHost();
            var port = getPort(implicit ? 443 : 80);
            var auth = getCmdLine().getOptionValue("http_auth", "basic");
            var method = getCmdLine().getOptionValue("http_method", "get");
            var action = getCmdLine().getOptionValue("http_action", "/");
            var headers = getCmdLine().getOptionProperties("http_header");
            var parameters = getCmdLine().getOptionProperties("http_parameter");
            var invalidData = getCmdLine().getOptionValue("http_invalid_data", "");
            var proxy = getCmdLine().getOptionValue("http_proxy", "");

            if (action.charAt(0) != '/')
                action = "/" + action;

            if (invalidData.isEmpty()) {
                invalidData = "incorrect|invalid|failed";
            }

            var uri = new StringBuilder();
            uri.append(String.format("%s://%s:%s%s", prot, host, port, action));

            var httpClientBuilder = HttpClient.newBuilder()
                .sslContext(SSLUtils.GetSSLContext())
                .version(Version.HTTP_1_1);

            if (!proxy.isEmpty()) {
                var proxyElement = proxy.split(":");
                var proxyHost = proxyElement[0];
                var proxyPort = Integer.parseInt(proxyElement[1]);

                httpClientBuilder = httpClientBuilder
                    .proxy(ProxySelector.of(new InetSocketAddress(proxyHost, proxyPort)));
            }

            var httpClient = httpClientBuilder.build();

            var requestBuilder = HttpRequest.newBuilder()
                .header("Pragma", "no-cache")
                .header("Cache-Control", "no-cache");

            var headersIterator = headers.entrySet().iterator();

            while(headersIterator.hasNext()) {
                var keyValue = headersIterator.next(); 
                var key = String.valueOf(keyValue.getKey());
                var value = String.valueOf(keyValue.getValue());  
                requestBuilder = requestBuilder.header(key, value);
            }

            var parametersSB = new StringBuilder();
            var parametersIterator = parameters.entrySet().iterator();

            while(parametersIterator.hasNext()) {
                var keyValue = parametersIterator.next();
                var parameter = String.format("%s=%s", keyValue.getKey(), keyValue.getValue());
                parameter = parameter.replace("$USER$", user);
                parameter = parameter.replace("$PASS$", pass);
                parametersSB.append(parameter);

                if (parametersIterator.hasNext())
                    parametersSB.append("&");
            }

            if (auth.equals("basic")) {
                    var userPass = String.format("%s:%s", user, pass);
                    var userPassEncoder = Base64.getEncoder().encodeToString(userPass.getBytes());

                    var httpRequest = requestBuilder
                        .uri(new URI(uri.toString()))
                        .header("Authorization", String.format("Basic %s", userPassEncoder))
                        .build();

                    var httpResponse = httpClient.send(httpRequest, BodyHandlers.ofString());

                    if (httpResponse.statusCode() == 200)
                        return true;
            } else if (auth.equals("form")) {
                if (method.equals("get")) {
                    if (parameters.size() > 0) {
                        uri.append("?");
                        uri.append(parametersSB.toString());
                    }

                    var httpRequest = requestBuilder
                        .uri(new URI(uri.toString()))
                        .build();

                    var httpResponse = httpClient.send(httpRequest, BodyHandlers.ofString());

                    var body = httpResponse.body();

                    if (httpResponse.statusCode() == 200 && body.length() > 0 && 
                        isValid(body, invalidData)) {
                        return true;
                    }
                } else if (method.equals("post")) {
                    var httpRequest = requestBuilder
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .uri(new URI(uri.toString()))
                        .POST(BodyPublishers.ofString(parametersSB.toString()))
                        .build();

                    var httpResponse = httpClient.send(httpRequest, BodyHandlers.ofString());

                    if (httpResponse.statusCode() == 302) {
                        var cookie = String.join(";", httpResponse.headers().allValues("Set-Cookie"));

                        httpRequest = requestBuilder
                            .header("Cookie", cookie)
                            .uri(new URI(uri.toString()))
                            .GET()
                            .build();

                        httpResponse = httpClient.send(httpRequest, BodyHandlers.ofString());
                    }

                    var body = httpResponse.body();

                    if (httpResponse.statusCode() == 200 && body.length() > 0 && 
                        isValid(body, invalidData)) {
                        return true;
                    }
                } else if (method.equals("get_post")) {
                    var httpRequest = requestBuilder
                        .uri(new URI(uri.toString()))
                        .GET()
                        .build();

                    var httpResponse = httpClient.send(httpRequest, BodyHandlers.ofString());
                    
                    if (httpResponse.statusCode() == 200) {
                        var body = httpResponse.body();

                        var document = Jsoup.parse(body);

                        var elements = document.select("input,button,select");
                        var elementsIter = elements.iterator();

                        if (parametersSB.length() > 0)
                            parametersSB.append("&");

                        while (elementsIter.hasNext()) {
                            var element = elementsIter.next();
                            var name = element.attr("name");
                            var value = "";
                            
                            if (element.tagName().equals("select")) {
                                value = element.select("option").val();
                            } else {
                                value = element.attr("value");
                            }
                                
                            if (parametersSB.indexOf(name) == -1) {
                                var nameLowerCase = name.toLowerCase();

                                if (nameLowerCase.contains("user") || nameLowerCase.contains("login")) {
                                    var parameter = String.format("%s=%s", name, user);
                                    parametersSB.append(parameter);
                                } else if (nameLowerCase.contains("pass")) {
                                    var parameter = String.format("%s=%s", name, pass);
                                    parametersSB.append(parameter);
                                } else {
                                    var parameter = String.format("%s=%s", name, value);
                                    parametersSB.append(parameter);
                                }
    
                                if (elementsIter.hasNext())
                                    parametersSB.append("&");
                            }
                        }

                        var cookie = String.join(";", httpResponse.headers().allValues("Set-Cookie"));

                        httpRequest = requestBuilder
                            .header("Content-Type", "application/x-www-form-urlencoded")
                            .header("Cookie", cookie)
                            .uri(new URI(uri.toString()))
                            .POST(BodyPublishers.ofString(parametersSB.toString()))
                            .build();

                        httpResponse = httpClient.send(httpRequest, BodyHandlers.ofString());

                        if (httpResponse.statusCode() == 302) {
                            cookie = String.join(";", httpResponse.headers().allValues("Set-Cookie"));

                            httpRequest = requestBuilder
                                .header("Cookie", cookie)
                                .uri(new URI(uri.toString()))
                                .GET()
                                .build();

                            httpResponse = httpClient.send(httpRequest, BodyHandlers.ofString());
                        }

                        body = httpResponse.body();

                        if (httpResponse.statusCode() == 200 && body.length() > 0 && 
                            isValid(body, invalidData)) {
                            return true;
                        }
                    }
                }
            }

            return false;
        } catch(Exception ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    private boolean isValid(String body, String invalidData) {
        var invalidList = invalidData.split("\\|");

        for (var invalidText : invalidList)
            if (body.toLowerCase().contains(invalidText))
                return false;

        return true;
    }
}
