package com.devjpcb.service;

public class RunnableService implements Runnable {
    private RunnableServiceParameters parameters;
    
    public RunnableService(RunnableServiceParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void run() {
        var service = parameters.getService();
        var index = parameters.getExecutorIndex().incrementAndGet();
        var user = parameters.getUser();
        var pass = parameters.getPass();
        var verbose = service.getCmdLine().hasOption("verbose");

        var message = new StringBuilder();
        message.append("[%d] username: %s");
        if(pass != null)
            message.append(" password: %s");
        message.append(" - %s");
        message.append(System.lineSeparator());

        try {
            var result = service.performTest(user, pass);
            
            if (result || verbose) {
                var validateMessage = result ? "valid" : "invalid";
                if(pass != null)
                    System.out.printf(message.toString(), index, user, pass, validateMessage);
                else
                    System.out.printf(message.toString(), index, user, validateMessage);
            }
        } catch (ServiceException ex) {
            String messageError = String.format("error: %s", ex.getMessage());

            if(pass != null)
                System.out.printf(message.toString(), index, user, pass, messageError);
            else
                System.out.printf(message.toString(), index, user, messageError);
        }
    }
}