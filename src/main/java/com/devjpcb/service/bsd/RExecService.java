package com.devjpcb.service.bsd;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;

import org.apache.commons.net.bsd.RExecClient;

public class RExecService extends AbstractService {
    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var rexecClient = new RExecClient();

        try {
            rexecClient.connect(getHost(), getPort(rexecClient.getDefaultPort()));
            try {
                rexecClient.rexec(user, pass, "ls");
                return true;
            } catch(IOException ex) {
                return false;
            }
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (rexecClient.isConnected()) {
                try {
                    rexecClient.disconnect();
                } catch (IOException e) { }
            }
        }
    }
    
}
