package com.devjpcb.service.bsd;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;

import org.apache.commons.net.bsd.RLoginClient;

public class RLoginService extends AbstractService {
    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var rLoginClient = new RLoginClient();

        try {
            rLoginClient.connect(getHost(), getPort(rLoginClient.getDefaultPort()));
            try {
                rLoginClient.rexec(user, pass, "ls");
                return true;
            } catch(IOException ex) {
                return false;
            }
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (rLoginClient.isConnected()) {
                try {
                    rLoginClient.disconnect();
                } catch (IOException e) { }
            }
        }
    }
    
}
