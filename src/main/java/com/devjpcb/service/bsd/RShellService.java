package com.devjpcb.service.bsd;

import java.io.IOException;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;

import org.apache.commons.net.bsd.RCommandClient;

public class RShellService extends AbstractService {
    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var rcommandClient = new RCommandClient();

        try {
            rcommandClient.connect(getHost(), getPort(rcommandClient.getDefaultPort()));
            try {
                rcommandClient.rexec(user, pass, "ls");
                return true;
            } catch(IOException ex) {
                return false;
            }
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            if (rcommandClient.isConnected()) {
                try {
                    rcommandClient.disconnect();
                } catch (IOException e) { }
            }
        }
    }
    
}
