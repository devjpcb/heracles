package com.devjpcb.service;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

public interface Service {
    String getName();
    String getDescription();
    void prepareOptions(Options options);
    void setCmdLine(CommandLine cmdline);
    void setup() throws ServiceException;
    CommandLine getCmdLine();
    boolean hasPassword();
    boolean performTest(String user, String pass) throws ServiceException;
    List<String> subServices();
}
