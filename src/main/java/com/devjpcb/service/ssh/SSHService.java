package com.devjpcb.service.ssh;

import com.devjpcb.service.AbstractService;
import com.devjpcb.service.ServiceException;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;

public class SSHService extends AbstractService {
    @Override
    public boolean performTest(String user, String pass) throws ServiceException {
        var jsch = new JSch();
        try {
            var session = jsch.getSession(user, getHost(), getPort(22));
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(pass);
            session.connect();
            if (session.isConnected())
                session.disconnect();
            return true;
        } catch (JSchException e) {
            return false;
        }
    }
}
